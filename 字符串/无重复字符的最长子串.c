//https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/
/*
给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度
示例 1:

输入: s = "abcabcbb"
输出: 3 
解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
*/


int lengthOfLongestSubstring(char * s){
    int j=0, max=0;
    int L=0, R=0;

    //从索引[L,R)字符表示当前不重复字符串，
    //不管有无重复，每次大循环R都向后滑动一个字符
    //当第R个字符不和里面的重复，L不变，随着R增大，字符串加长
    //当第R个同其中第j个相同，则L移到j的下一个，即j+1
    //计算个数时，因为要算上第R个，所以R-L+1
    for(R=0; R<strlen(s); R++){
        for(j=L; j<R; j++){
            if(s[j] == s[R]){
                L = j + 1;
                break;
            }
        }
        max = max < (R-L+1) ? R-L+1 : max;
    }
    return max;
}