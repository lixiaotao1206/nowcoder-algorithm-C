/*
https://www.nowcoder.com/practice/c3120c1c1bc44ad986259c0cf0f0b80e?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0&sourceUrl=
对于一个长度为 n 字符串，我们需要对它做一些变形。
首先这个字符串中包含着一些空格，就像"Hello World"一样，
然后我们要做的是把这个字符串中由空格隔开的单词反序，同时反转每个字符的大小写。
比如"Hello World"变形后就变成了"wORLD hELLO"。
*/
class Solution {
public:
    string trans(string s, int n) {
        // write code here
        stack<string> sk;
        string str;
        //先在后面追加空格，防止漏掉最后一个单词
        s.push_back(' ');    
        //大小写转换，入栈
        for(int i=0; i<=n; i++){
            if(s[i] != ' '){
                if(s[i]>='a' && s[i]<='z'){
                    str.push_back(s[i] - 'a' + 'A');
                }
                else if(s[i]>='A' && s[i]<='Z'){
                    str.push_back(s[i] - 'A' + 'a');
                }
            }
            else{
                sk.push(str);
                str = "";
            }
        }
        str = "";
        //出栈
        while(!sk.empty()){
            str += sk.top(); 
            sk.pop();
            str += ' ';
        }
        //去除最后补得空格
        str.pop_back();
        return str;
    }
};