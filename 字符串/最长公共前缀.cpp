/*
https://www.nowcoder.com/practice/28eb3175488f4434a4a6207f6f484f47?tpId=295&tqId=732&ru=%2Fpractice%2Fc3120c1c1bc44ad986259c0cf0f0b80e&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=
*/
class Solution {
public:
    /**
     * 
     * @param strs string字符串vector 
     * @return string字符串
     */
    string longestCommonPrefix(vector<string>& strs) {
        // write code here
        string prestr = "";
        if(strs.size()==0){
            return "";
        }
        string str = strs[0];
        //依照第一个字符串，遍历每一个字符
        for(int i=0; i<str.size(); i++){
            //按照每个字符，所有字符串对照一遍
            for(int j=1; j<strs.size(); j++){
                //长度不够或者字符不同就直接返回
                if(strs[j].size() <= i || str.at(i) != strs[j].at(i)){
                    return prestr;
                }
            }
            //该字符在这个位置都存在，就加到prestr
            prestr.push_back(str.at(i));
        }
        return prestr;
    }
};