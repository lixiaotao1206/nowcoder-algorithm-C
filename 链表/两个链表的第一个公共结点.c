//https://www.nowcoder.com/practice/6ab1d9a29e88450685099d45c9e31e46?tpId=295&tqId=23257&ru=%2Fpractice%2F886370fe658f41b498d40fb34ae76ff9&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 * 
 * @param pHead1 ListNode类 
 * @param pHead2 ListNode类 
 * @return ListNode类
 */
//找到两个链表长度差值，
//先让长的走差值步
//俩再一起走，见面的第一个就是第一个公共节点
struct ListNode* FindFirstCommonNode(struct ListNode* pHead1, struct ListNode* pHead2 ) {
    // write code here
    struct ListNode* p1 = pHead1;
    struct ListNode* p2 = pHead2;
    int diffCount = 0;
    if(!p1 || !p2){
        return NULL;
    }
    
    while(p1 && p2){
        p1 = p1->next;
        p2 = p2->next;
    }
    
    //pHead1短
    if(!p1){
        while(p2){
            p2 = p2->next;
            diffCount++;
        }
        p1 = pHead1;
        p2 = pHead2;
    }
     //pHead1短
    else{
        while(p1){
            p1 = p1->next;
            diffCount++;
        }
        p1 = pHead2;
        p2 = pHead1;
    }
    //上面的目的只是让p1指向短的，p2指向长的
    //下面是先让长的走diffCount步
    while(diffCount-- > 0){
        p2 = p2->next;
    }
    //一起走
    while(p1 != p2){
        p1 = p1->next;
        p2 = p2->next;
    }
    return p1;
}