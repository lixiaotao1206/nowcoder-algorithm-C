//https://www.nowcoder.com/practice/3fed228444e740c8be66232ce8b87c2f?tpId=295&tqId=1008769&ru=%2Fpractice%2Ff23604257af94d939848729b1a5cda08&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 * 
 * @param head ListNode类 the head
 * @return bool布尔型
 */
bool isPail(struct ListNode* head ) {
    // write code here
    struct ListNode* p = head;
    struct ListNode* pre = NULL, *cur, *nxt;
    int i = 0, count = 0;
    //计算链表节点总数
    while(p){
        p = p->next;
        count++;
    }
    //找到链表中间节点
    p = head;
    for(i=0; i<count/2+1; i++){
        p = p->next;
    }
    //将后半部分反转
    cur = p;
    while(cur){
        nxt = cur->next;
        cur->next = pre;
        pre = cur;
        cur = nxt;
    }
    //比较每个节点值
    p = head;
    while(p && pre && p->val == pre->val){
        p = p->next;
        pre = pre->next;
    }
    //没走到最后说明中间有不相等的，则返回false
    if(p && pre){
        return false;
    }
    return true;
}