//https://www.nowcoder.com/practice/253d2c59ec3e4bc68da16833f79a38e4?tpId=295&tqId=23449&ru=%2Fpractice%2F650474f313294468a4ded3ce0f7898b9&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param pHead ListNode类 
 * @return ListNode类
 */
struct ListNode* EntryNodeOfLoop(struct ListNode* pHead ) {
    // write code here
    struct ListNode* p1 = pHead;
    struct ListNode* p2 = pHead;
    char isCycle = 0;
    //判断是否有环，从起点开始，p1每次走一步，p2每次走2步，有环的话会相遇，此时p2路程=2*p1
    while(p1 && p2 && p2->next){
        p1 = p1->next;
        p2 = p2->next->next;
        if(p1 == p2){
            isCycle = 1;
            break;
        }
    }
    //无环就直接返回null
    if(!isCycle)
        return NULL;
    //找环，p1从头开始走，p2在相遇点开始走
    //https://zhuanlan.zhihu.com/p/103626709
    p1 = pHead;
    while(p1 != p2){
        p1 = p1->next;
        p2 = p2->next;
    }
    return p1;
}