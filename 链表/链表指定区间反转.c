//https://www.nowcoder.com/practice/b58434e200a648c589ca2063f1faf58c?tpId=295&tqId=654&ru=%2Fpractice%2F75e878df47f24fdc9dc3e400ec6058ca&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param head ListNode类 
 * @param m int整型 
 * @param n int整型 
 * @return ListNode类
 */
struct ListNode* reverseBetween(struct ListNode* head, int m, int n ) {
    // write code here
    struct ListNode* pa = NULL;
    struct ListNode* pb = NULL;
    struct ListNode* pc = NULL;
    struct ListNode* pre = NULL;
    struct ListNode* cur = head;
    struct ListNode* nxt = NULL;
    int i = 1;
    
    if(!head){
        return NULL;
    }
    //m==n表示什么也不做
    if(m == n){
        return head;
    }
    //寻找区间前节点pa，区间起始节点pb，区间结束节点pc
    while(cur){
        if(i == m){
            pa = pre;
            pb = cur;
        }
        if(i == n){
            pc = cur;
            break;
        }
        pre = cur;
        cur = cur->next;
        i++;
    }
    //将区间反转
    pre = pa;
    cur = pb;
    while(cur != pc){
        nxt = cur->next;
        cur->next = pre;
        pre = cur;
        cur = nxt;
    }
    //将区间和两端连接
    pb->next = pc->next;
    pc->next = pre;
    if(pa){
        //若总攻两个节点，此时pa为null
        pa->next = pc;
    }
    
    //从头开始反转，则返回尾部
    if(m == 1){
        return cur;
    }
    return head;
}