//https://www.nowcoder.com/practice/886370fe658f41b498d40fb34ae76ff9?tpId=295&tqId=1377477&ru=%2Fpractice%2F253d2c59ec3e4bc68da16833f79a38e4&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param pHead ListNode类 
 * @param k int整型 
 * @return ListNode类
 */
struct ListNode* FindKthToTail(struct ListNode* pHead, int k ) {
    // write code here
    struct ListNode* p1 = pHead;
    struct ListNode* p2 = pHead;
    int i = 1;
    //先让p1走k步
    if(!pHead){
        return NULL;
    }
    while(i++ <= k){
        p1 = p1->next;
        if(!p1){
            break;
        }
    }
    if(i <= k){
        return NULL;
    }
    //然后p1和p2一起走，p1到头时，p2正好剩余k步
    while(p1){
        p1 = p1->next;
        p2 = p2->next;
    }
    return p2;
}