//https://www.nowcoder.com/practice/71cef9f8b5564579bf7ed93fbe0b2024?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0&sourceUrl=%2Fexam%2Foj

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param head ListNode类 
 * @return ListNode类
 */
struct ListNode* deleteDuplicates(struct ListNode* head ) {
    // write code here
    //创建新的起始点
    struct ListNode newhead = {-1001, head};
    struct ListNode *pre = &newhead, *cur = head, *nxt;
    bool isDel = false; //是否需要删除cur
    if(!head || !head->next){
        return head;
    }
    //每次删除nxt
    nxt = cur->next;
    while(cur && nxt){
        if(nxt->val == cur->val){
            isDel = true;
            nxt = nxt->next;
            cur->next = nxt;
            continue;
        }
        if(isDel){
            isDel = false;
            pre->next = nxt;
            cur = nxt;
            nxt = nxt->next;
            continue;
        }
        pre = cur;
        cur = nxt;
        nxt = nxt->next;
    }
    if(isDel){
        pre->next = nxt;
    }
    return newhead.next;
}