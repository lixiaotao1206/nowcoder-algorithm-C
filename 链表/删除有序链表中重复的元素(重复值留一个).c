//https://www.nowcoder.com/practice/c087914fae584da886a0091e877f2c79?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0&sourceUrl=%2Fexam%2Foj

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param head ListNode类 
 * @return ListNode类
 */
struct ListNode* deleteDuplicates(struct ListNode* head ) {
    // write code here
    struct ListNode *p1 = head, *p2 = head;
    if(!head || !head->next){
        return head;
    }
    p2 = head->next;
    //保留第一个重复值
    while(p2){
        if(p1->val == p2->val){
            p2 = p2->next;
            p1->next = p2;
            continue;
        }
        p1 = p1->next;
        p2 = p2->next;
    }
    return head;
}