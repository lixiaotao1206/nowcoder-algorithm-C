//https://www.nowcoder.com/practice/f23604257af94d939848729b1a5cda08?tpId=295&tqId=1008897&ru=%2Fpractice%2F6ab1d9a29e88450685099d45c9e31e46&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 * 
 * @param head ListNode类 the head node
 * @return ListNode类
 */
struct ListNode* sortInList(struct ListNode* head ) {
    // write code here
    struct ListNode *m = NULL;
    struct ListNode *n = NULL;
    int tmp = 0;
    //和数组排序一样
    for(m=head; m; m=m->next){
        for(n=m->next; n; n=n->next){
            if(m->val > n->val){
                tmp = m->val;
                m->val = n->val;
                n->val = tmp;
            }
        }
    }
    return head;
}