//https://www.nowcoder.com/practice/d8b6b4358f774294a89de2a6ac4d9337?tpId=295&tqId=23267&ru=%2Fpractice%2Fb58434e200a648c589ca2063f1faf58c&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/*
1->3->5
2->4->6

=> 1->2->3->4->5->6
*/

/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 * 
 * @param pHead1 ListNode类 
 * @param pHead2 ListNode类 
 * @return ListNode类
 */
struct ListNode* Merge(struct ListNode* pHead1, struct ListNode* pHead2 ) {
    // write code here
    if(!pHead1) return pHead2;
    if(!pHead2) return pHead1;
    
    if(pHead1->val < pHead2->val){
        pHead1->next = Merge(pHead1->next, pHead2);
        return pHead1;
    }
    else{
        pHead2->next = Merge(pHead2->next, pHead1);
        return pHead2;
    }
}