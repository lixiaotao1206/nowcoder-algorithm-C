/*
https://www.nowcoder.com/practice/89865d4375634fc484f3a24b7fe65665?tpId=295&tqId=658&ru=%2Fpractice%2F11ae12e8c6fe48f883cad618c2e81475&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=
给出一个有序的整数数组 A 和有序的整数数组 B ，请将数组 B 合并到数组 A 中，变成一个有序的升序数组
注意：
1.保证 A 数组有足够的空间存放 B 数组的元素， A 和 B 中初始的元素数目分别为 m 和 n，A的数组空间大小为 m+n
2.不要返回合并的数组，将数组 B 的数据合并到 A 里面就好了，且后台会自动将合并后的数组 A 的内容打印出来，所以也不需要自己打印
3. A 数组在[0,m-1]的范围也是有序的
*/
class Solution {
public:
    void merge(int A[], int m, int B[], int n) {
        int i = m-1;
        int j = n-1;
        int p = m+n-1;
        //从最末尾往前排，A与B选大的依次放进去
        while(i>=0 && j>=0){
            if(A[i] > B[j]){
                A[p] = A[i];
                i--;
            }
            else{
                A[p] = B[j];
                j--;
            }
            p--;
        }
        //有可能A的第一个值大于B的第一个值，此时B放不完，继续放
        while(j>=0){
            A[p] = B[j];
            j--;
            p--;
        }
    }
};