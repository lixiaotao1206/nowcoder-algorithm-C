/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 ===========================================================
 https://www.nowcoder.com/practice/20ef0972485e41019e39543e8e895b7f?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0
 给出一个整型数组 numbers 和一个目标值 target，请在数组中找出两个加起来等于目标值的数的下标，
 返回的下标按升序排列。
（注：返回的数组下标从1开始算起，保证target一定可以由数组里面2个数字相加得到）
===================================================================
 * 
 * @param numbers int整型一维数组 
 * @param numbersLen int numbers数组长度
 * @param target int整型 
 * @return int整型一维数组
 * @return int* returnSize 返回数组行数
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int* twoSum(int* numbers, int numbersLen, int target, int* returnSize ) {
    // write code here
    int *array = malloc(sizeof(int)*2);
    *returnSize = 0;
    for(int i=0; i<numbersLen; i++){
        if(numbers[i] > target){
            continue;
        }
        for(int j=i+1; j<numbersLen; j++){
            if(numbers[i] + numbers[j] == target){
                *array = i+1;
                *(array+1) = j+1;
                *returnSize += 2;
                return array;
            }
        }

    }
    return 0;
}