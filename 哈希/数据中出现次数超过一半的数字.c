/**
 * 
 ============================================
 https://www.nowcoder.com/practice/e8a1b01a2df14cb2b228b30ee6a92163?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0
 给一个长度为 n 的数组，数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。
例如输入一个长度为9的数组[1,2,3,2,2,2,5,4,2]。由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。

数据范围：n \le 50000n≤50000，数组中元素的值 0 \le val \le 100000≤val≤10000
要求：空间复杂度：O(1)O(1)，时间复杂度 O(n)O(n)
输入描述：
保证数组输入非空，且保证有解
=============================================
候选法（最优解）
假如数组中存在出现次数大于一半的众数，那么众数一定大于数组的长度的一半。
思想就是：如果两个数不相等，就消去这两个数，最坏情况下，每次消去一个众数和一个非众数，那么如果存在众数，最后留下的数肯定是众数。

具体做法：
初始化：候选人cond = -1， 候选人的投票次数cnt = 0
遍历数组，如果cnt=0， 表示没有候选人，则选取当前数为候选人，++cnt
如果cnt > 0, 表示有候选人，如果当前数=cond，则++cnt，否则--cnt
直到数组遍历完毕，若那个众数次数超过一半，经过两两PK，
最终留下来的一定是那个众数。
最后检查cond次数是否超过一半
 * @param numbers int整型一维数组 
 * @param numbersLen int numbers数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int MoreThanHalfNum_Solution(int* numbers, int numbersLen ) {
    // write code here
    int cond = -1;
    int cnt = 0;
    //两两PK，找众数
    for(int i=0; i<numbersLen; i++){
        if(cnt == 0){
            cond = numbers[i];
            cnt ++;
        }
        else{
            if(numbers[i] == cond){
                cnt ++;
            }
            else{
                cnt --;
            }
        }
    }
    //return cond; //如果能保证一定存在次数大于一般的数，那就没必要下面的检验了
    //检查次数是否超过一半
    cnt = 0;
    for(int i=0; i<numbersLen; i++){
        if(cond == numbers[i]){
            cnt ++;
        }
    }
    if(cnt * 2 > numbersLen){
        return cond;
    }
    return 0;
}