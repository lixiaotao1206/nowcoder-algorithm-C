/*
https://www.nowcoder.com/practice/7edf70f2d29c4b599693dc3aaeea1d31?tpId=295&tqId=693&ru=%2Fpractice%2F4edf6e6d01554870a12f218c94e8a299&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=
给定一个m x n大小的矩阵（m行，n列），按螺旋的顺序返回矩阵中的所有元素。
*/

class Solution {
public:
    vector<int> spiralOrder(vector<vector<int> > &matrix) {
        int top = 0;
        int bottom = 0;
        int left = 0;
        int right = 0;
        
        int i;
        
        vector<int> vec;
        if(matrix.size() == 0){
            return vec;
        }

        bottom = matrix.size()-1;
        right = matrix.at(0).size()-1;
        while(left < (matrix.at(0).size()+1)/2 && top < (matrix.size()+1)/2){
            //从左向右
            for(i=left; i<=right; i++){
                vec.push_back(matrix.at(top).at(i));
            }
            //从上到下
            for(i=top+1; i<=bottom; i++){
                vec.push_back(matrix.at(i).at(right));
            }
            //从右到左
            for(i=right-1; top!=bottom && i>=left; i--){
                vec.push_back(matrix.at(bottom).at(i));
            }
            //从下到上
            for(i=bottom-1; left!=right && i>top; i--){
                vec.push_back(matrix.at(i).at(left));
            }
            top++;
            bottom--;
            left++;
            right--;
        }
        return vec;
    }
};



