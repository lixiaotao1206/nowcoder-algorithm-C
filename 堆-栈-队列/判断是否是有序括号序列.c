/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param s string字符串 
 * @return bool布尔型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
typedef struct _str_t str_t;
struct _str_t{
    char c;
    str_t* next;
};
static str_t *tail = NULL;

void pop(){
    str_t* nxt = tail->next;
    free(tail);
    tail = nxt;
}
void push(char c){
    str_t* m = (str_t*)malloc(sizeof(str_t));
    m->c = c;
    m->next = tail;
    tail = m;
}
char top(){
    if(!tail){
        return '\0';
    }
    return tail->c;
}
bool empty(){
    if(tail){
        return false;
    }
    return true;
}


/*
*	若遇到左括号入栈，若遇到右括号，如果栈顶不与之匹配，则错误，如果匹配，则出栈。
*/
bool isValid(char* s ) {
    // write code here
    char *p = s;
    if(*p == '\0'){
        return false;
    }
    push(*p);
    p++;
    while(*p != '\0'){
        if(*p == ')'){
            if(top() != '(')
            {
                return false;
            }
            pop();
        }
        else if(*p == ']'){
            if(top() != '[')
            {
                return false;
            }
            pop();
        }
        else if(*p == '}'){
            if(top() != '{')
            {
                return false;
            }
            pop();
        }
        else{
            push(*p);
        }
        p++;
    }
    if(empty()){
        return true;
    }
    return false;
}