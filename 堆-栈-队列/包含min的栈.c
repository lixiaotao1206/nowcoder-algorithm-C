/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param value int整型 
 * @return 无
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

typedef struct _st st_t;
struct _st{
    int val;
    st_t* next;
};
st_t *tail = NULL;

void push(int value ) {
    // write code here
    st_t* m = (st_t*)malloc(sizeof(st_t));
    m->val = value;
    m->next = tail;
    tail = m;
}

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param 无 
 * @return 无
 */
void pop() {
    // write code here
    st_t *t = NULL;
    if(!tail){
        return;
    }
    else{
        t = tail->next;
        free(tail);
        tail = t;
    }
}

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param 无 
 * @return int整型
 */
int top() {
    // write code here
    return tail->val;
}

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param 无 
 * @return int整型
 */
int min() {
    // write code here
    st_t *p = tail;
    int min = p->val;
    while(p->next){
        if(p->val < min){
            min = p->val;
        }
        p = p->next;
    }
    if(p->val < min){
        min = p->val;
    }
    return min;
}