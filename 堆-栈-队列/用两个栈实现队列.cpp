class MyQueue {
public:
    stack<int> stk_in;
    stack<int> stk_out;

    MyQueue() {

    }
    
    void push(int x) {
        stk_in.push(x);
    }
    
    int pop() {
        int t = 0; 
        if(stk_out.empty()){
            while(!stk_in.empty()){
                stk_out.push(stk_in.top());
                stk_in.pop();
            }
        }
        t = stk_out.top();
        stk_out.pop();
        return t;
    }
    
    int peek() {
        if(stk_out.empty()){
            while(!stk_in.empty()){
                stk_out.push(stk_in.top());
                stk_in.pop();
            }
        }
        return stk_out.top();
    }
    
    bool empty() {
        return stk_out.empty() && stk_in.empty();
    }
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */