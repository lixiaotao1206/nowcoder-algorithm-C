/**
 * 
https://www.nowcoder.com/practice/c6c7742f5ba7442aada113136ddea0c3?tpId=295&tqId=23255&ru=/practice/4bcf3081067a4d028f95acee3ddcd2b1&qru=/ta/format-top101/question-ranking
 * @param n int整型 
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int Fibonacci(int n ) {
    // write code here
    if(n == 1 || n == 2){
        return 1;
    }
    else{
        return Fibonacci(n-1) + Fibonacci(n-2);
    }
}