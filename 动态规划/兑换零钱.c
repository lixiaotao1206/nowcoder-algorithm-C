/**
 * 最少货币数
   https://www.nowcoder.com/practice/3911a20b3f8743058214ceaa099eeb45?tpId=295&tqId=988994&ru=/practice/166eaff8439d4cd898e3ba933fbc6358&qru=/ta/format-top101/question-ranking
 * @param arr int整型一维数组 the array
 * @param arrLen int arr数组长度
 * @param aim int整型 the target
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int minMoney(int* arr, int arrLen, int aim ) {
    // write code here
    int dp[aim + 1];
    //把dp数组全部定为最大值
    for(int i=0; i < aim+1; i++){
        dp[i] = aim + 1;
    }
    //总金额为0的时候所需钱币数一定是0
    dp[0] = 0;
    // 遍历目标值
    for (int i = 1; i <= aim; i++) {
        // 遍历钱币
        for (int j = 0; j < arrLen; j++) {
            //如果当前的钱币比目标值小就可以兑换
            if (arr[j] <= i) {
                dp[i] = dp[i] < (dp[i - arr[j]] + 1) ? 
                    dp[i] : (dp[i - arr[j]]+1);
                    
            }
        }
    }
    return dp[aim] > aim ? -1 : dp[aim];
}