/**
一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
数据范围：1 \leq n \leq 401≤n≤40
要求：时间复杂度：O(n)O(n) ，空间复杂度： O(1)O(1)
 * https://www.nowcoder.com/practice/8c82a5b80378478f9484d87d1c5f12a4?tpId=295&tqId=23261&ru=/practice/c6c7742f5ba7442aada113136ddea0c3&qru=/ta/format-top101/question-ranking
 * @param number int整型 
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int jumpFloor(int number ) {
    // write code here
    /*
    方法一：递归
    逆向思维。如果我从第n个台阶进行下台阶，下一步有2中可能，
    一种走到第n-1个台阶，一种是走到第n-2个台阶。
    所以f[n] = f[n-1] + f[n-2]. 那么初始条件了，f[0] = f[1] = 1。
    所以就变成了：f[n] = f[n-1] + f[n-2], 
    初始值f[0]=1, f[1]=1，目标求f[n] 看到公式很亲切，代码秒秒钟写完。
    */
//     if(number == 0 || number == 1){
//         return 1;
//     }
//     return jumpFloor(number-1) + jumpFloor(number-2);
    /*
    *方法二：记忆化搜索 
    *方法一存在许多重复计算，具体从网址链接看
    */
//     int func[50];
//     memset(func, 0, sizeof(func));
//     if(number==0 || number==1){
//         return 1;
//     }
//     //已经计算过的保存下来的不在重复计算，直接返回方法数量。
//     if(func[number] > 0){
//         return func[number];
//     }
//     else{
//         func[number] = jumpFloor(number-1) + jumpFloor(number-2);
//     }
//     return func[number];
    /*
    方法三：动态规划
    到台阶n的方法数 = 到台阶n-1方法数 + 到台阶n-2方法数
    */
    int a = 1; //number=0 一种方法
    int b = 1; //number=1 一种方法
    int c = 1;
    //从number=2开始会用到下面计算
    for(int i=2; i<=number; i++){
        c = a + b;
        a = b;
        b = c;
    }
    return c;
}