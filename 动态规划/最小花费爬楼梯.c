/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 * https://www.nowcoder.com/practice/6fe0302a058a4e4a834ee44af88435c7?tpId=295&tqId=2366451&ru=/practice/8c82a5b80378478f9484d87d1c5f12a4&qru=/ta/format-top101/question-ranking
 * 给定一个整数数组 cost \cost  ，其中 cost[i]\cost[i]  是从楼梯第i \i 个台阶向上爬需要支付的费用，下标从0开始。一旦你支付此费用，即可选择向上爬一个或者两个台阶。

你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。

请你计算并返回达到楼梯顶部的最低花费。

 * @param cost int整型一维数组 
 * @param costLen int cost数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int minCostClimbingStairs(int* cost, int costLen ) {
    // write code here
    int costsum_1 = 0;    //到阶梯0共花0元
    int costsum_2 = 0;    //到阶梯1共花0元
    int costsum = 0;
    //从阶梯2开始需要下面的计算，看是前1阶梯最小总和加前1阶梯价格与前2阶梯最小总和加前2阶梯价格那个小
    for(int i=2; i<=costLen; i++){
        costsum = (costsum_1 + cost[i-1]) > (costsum_2 + cost[i-2]) ? 
            (costsum_2 + cost[i-2]) : (costsum_1 + cost[i-1]);
        costsum_2 = costsum_1;    //向前滑动一阶梯价格，用于下次计算
        costsum_1 = costsum;
    }
    return costsum;
}