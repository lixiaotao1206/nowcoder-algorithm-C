/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 * https://www.nowcoder.com/practice/64b4262d4e6d4f6181cd45446a5821ec?tpId=295&tags=&title=&difficulty=0&judgeStatus=0&rp=0&sourceUrl=
 * 
 * @param prices int整型一维数组 
 * @param pricesLen int prices数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int maxProfit(int* prices, int pricesLen ) {
    // write code here
    int minPrice = prices[0];    //当前最低价
    int maxProfit = 0;         //当前能获得的最大利润
    for(int i=1; i<pricesLen; i++){
        maxProfit = prices[i]-minPrice > maxProfit ? 
            prices[i]-minPrice : maxProfit;
        minPrice = prices[i] < minPrice ? prices[i] : minPrice;
    }
    return maxProfit;
}