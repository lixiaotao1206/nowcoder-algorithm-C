/**
 * https://www.nowcoder.com/practice/459bd355da1549fa8a49e350bf3df484?tpId=295&tqId=23259&ru=/practice/3911a20b3f8743058214ceaa099eeb45&qru=/ta/format-top101/question-ranking
 * @param array int整型一维数组 
 * @param arrayLen int array数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int FindGreatestSumOfSubArray(int* array, int arrayLen ) {
    // write code here
    int sum = array[0];
    int max = array[0];
    //判断当前值array[i]之前的最大和是否为正，正则累加，负则舍去重新从
    //array[i]开始
    for(int i=1; i< arrayLen; i++){
        sum = sum > 0 ? sum+array[i] : array[i];
        max = max < sum ? sum : max;
    }
    return max;
}