/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 * https://www.nowcoder.com/practice/166eaff8439d4cd898e3ba933fbc6358?tpId=295&tqId=685&ru=/exam/oj&qru=/ta/format-top101/question-ranking&sourceUrl=%2Fexam%2Foj%3Ftab%3D%25E7%25AE%2597%25E6%25B3%2595%25E7%25AF%2587%26topicId%3D295
 * 
 * @param m int整型 
 * @param n int整型 
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int uniquePaths(int m, int n ) {
    // write code here
    if(m==1 || n==1){
        return 1;
    }
    return uniquePaths(m-1, n) + uniquePaths(m, n-1);
}
/*
        1,  1,  1,  1
        1,  2,  3,  4
        1,  3,  6,  10
        1,  4,  10, 20
*/