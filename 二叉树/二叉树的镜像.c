//https://www.nowcoder.com/practice/a9d0ecbacef9410ca97463e4a5c83be7?tpId=295&tqId=1374963&ru=%2Fpractice%2F7298353c24cc42e3bd5f0e0bd3d1d759&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj

/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param pRoot TreeNode类 
 * @return TreeNode类
 */
struct TreeNode* Mirror(struct TreeNode* pRoot ) {
    if (!pRoot)
    return NULL;
  //递归
  struct TreeNode *temp;
  temp = pRoot->left;
  pRoot->left = pRoot->right;
  pRoot->right = temp;
 
  pRoot->left = Mirror(pRoot->left);
  pRoot->right = Mirror(pRoot->right);
 
  return pRoot;
}