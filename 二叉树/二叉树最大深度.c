//https://www.nowcoder.com/practice/8a2b2bf6c19b4f23a9bdb9b233eefa73?tpId=295&tqId=642&ru=%2Fpractice%2F1291064f4d5d4bdeaefbf0dd47d78541&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj

/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
static void maxDepth_in(struct TreeNode* root, int curRow, int* max) {
    if(!root){
        return;
    }
    if(curRow > *max){
        *max = curRow;
    }
    maxDepth_in(root->left, curRow+1, max);
    maxDepth_in(root->right, curRow+1, max);
}
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param root TreeNode类 
 * @return int整型
 */
int maxDepth(struct TreeNode* root ) {
    int max = 0;
    maxDepth_in(root, 1, &max);
    return max;
}