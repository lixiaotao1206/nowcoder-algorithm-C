//https://www.nowcoder.com/practice/8b3b95850edb4115918ecebdf1b4d222?tpId=295&tqId=23250&ru=%2Fpractice%2Fa9d0ecbacef9410ca97463e4a5c83be7&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
unsigned int getDeepth(struct TreeNode* pRoot){
    unsigned int LD, RD;
    if(!pRoot){
        return 0;
    }
    LD = getDeepth(pRoot->left);
    RD = getDeepth(pRoot->right);
    return (LD > RD ? LD : RD) + 1;
}
/**
 * 
 * @param pRoot TreeNode类 
 * @return bool布尔型
 */
bool IsBalanced_Solution(struct TreeNode* pRoot ) {
    // write code here
    if(!pRoot){
        return true;
    }
    if(abs(getDeepth(pRoot->left) - getDeepth(pRoot->right)) > 1){
        return false;
    }
    return IsBalanced_Solution(pRoot->left) && IsBalanced_Solution(pRoot->right);
}