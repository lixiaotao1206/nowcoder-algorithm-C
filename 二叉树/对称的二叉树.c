//https://www.nowcoder.com/practice/ff05d44dfdb04e1d83bdbdab320efbcb?tpId=295&tqId=23452&ru=%2Fpractice%2F508378c0823c423baa723ce448cbfd0c&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
bool isMirror(struct TreeNode* left, struct TreeNode*right){
    //对称：左右相等，左边的左边和右边的右边相等，左边的右边和右边的左边相等
    if(!left && right){
        return false;
    }
    else if(left && !right){
        return false;
    }
    else if(!left && !right){
        return true;
    }
    else{
        if(left->val != right->val){
            return false;
        }
        else{
            return isMirror(left->left, right->right) && 
                        isMirror(left->right, right->left);
        }
    }
}
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param pRoot TreeNode类 
 * @return bool布尔型
 */
bool isSymmetrical(struct TreeNode* pRoot ) {
    // write code here
    if(!pRoot){
        return true;
    }
    return isMirror(pRoot->left, pRoot->right);
}