//https://www.nowcoder.com/practice/5e2135f4d2b14eb8a5b06fab4c938635?tpId=295&tqId=2291302&ru=%2Fpractice%2Fabc3fe2ce8e146608e868a70efebf62e&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
static void traversal(struct TreeNode* root, int* arr, int* returnSize){
    if(!root){
        return;
    }
    //root->left->right
    arr[*returnSize] = root->val;
    (*returnSize) ++;
    traversal(root->left, arr, returnSize);
    traversal(root->right, arr, returnSize);
}
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 
 * 
 * @param root TreeNode类 
 * @return int整型一维数组
 * @return int* returnSize 返回数组行数
 */
int* preorderTraversal(struct TreeNode* root, int* returnSize ) {
    // write code here
    int *arr = (int*)malloc(sizeof(int)*100);
    *returnSize = 0;
    traversal(root, arr, returnSize);
    return arr;
}