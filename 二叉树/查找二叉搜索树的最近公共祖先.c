/**
 * struct TreeNode {
 * int val;
 * struct TreeNode *left;
 * struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param root TreeNode类 
 * @param p int整型 
 * @param q int整型 
 * @return int整型
 */
int lowestCommonAncestor(struct TreeNode* root, int p, int q ) {
    struct TreeNode* cur = root;
    while(cur){
        if(p < cur->val && q < cur->val){
            cur = cur->left;
        }
        else if(p > cur->val && q > cur->val){
            cur = cur->right;
        }
        else{
            break;
        }
    }
    return cur->val;
}