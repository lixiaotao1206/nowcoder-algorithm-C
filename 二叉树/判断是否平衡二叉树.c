/**
 * struct TreeNode {
 *	int val;
 *	struct TreeNode *left;
 *	struct TreeNode *right;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
unsigned int getDeepth(struct TreeNode* pRoot){
    unsigned int LD, RD;
    if(!pRoot){
        return 0;
    }
    LD = getDeepth(pRoot->left);
    RD = getDeepth(pRoot->right);
    return (LD > RD ? LD : RD) + 1;
}
/**
 * 
 * @param pRoot TreeNode类 
 * @return bool布尔型
 */
bool IsBalanced_Solution(struct TreeNode* pRoot ) {
    // write code here
    if(!pRoot){
        return true;
    }
    if(abs(getDeepth(pRoot->left) - getDeepth(pRoot->right)) > 1){
        return false;
    }
    return IsBalanced_Solution(pRoot->left) && IsBalanced_Solution(pRoot->right);
}