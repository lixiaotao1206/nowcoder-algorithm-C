//https://www.nowcoder.com/practice/9f3231a991af4f55b95579b44b7a01ba?tpId=295&tqId=23269&ru=%2Fpractice%2Fd3df40bd23594118b57554129cadf47b&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj
/**
 * 
 * @param rotateArray int整型一维数组 
 * @param rotateArrayLen int rotateArray数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
/*
    3，4，5，1，2  -》  返回1
    找最大值的下一个值
    1,2,3,4,5
    若最大值在最后，则返回第一个值，最小值
*/
int minNumberInRotateArray(int* rotateArray, int rotateArrayLen ) {
    // write code here
    int i, max = 0;
    for(i=0; i<rotateArrayLen; i++){
        if(rotateArray[i] >= max){
            max = rotateArray[i];
        }
        else{
            return rotateArray[i];
        }
    }
    return rotateArray[0];
}