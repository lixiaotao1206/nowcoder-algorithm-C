//https://www.nowcoder.com/practice/d3df40bd23594118b57554129cadf47b?tpId=295&tqId=1499549&ru=%2Fpractice%2F8c82a5b80378478f9484d87d1c5f12a4&qru=%2Fta%2Fformat-top101%2Fquestion-ranking&sourceUrl=%2Fexam%2Foj

/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param nums int整型一维数组 
 * @param numsLen int nums数组长度
 * @param target int整型 
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int search(int* nums, int numsLen, int target ) {
    // write code here
    int begin = 0, end = numsLen-1, i = 0;
    if(numsLen <= 0){
        return -1;
    }

    while(begin <= end){
        i = (begin + end)/2;
        if(nums[i] > target){
            end = i-1;
        }
        else if(nums[i] < target){
            begin = i+1;
        }
        else{
            return i;
        }
    }

    return -1;
}